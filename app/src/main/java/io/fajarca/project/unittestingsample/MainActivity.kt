package io.fajarca.project.unittestingsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.button.MaterialButton
import io.fajarca.project.unittestingsample.movie.presentation.MovieListActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    private fun setupView() {
        val button = findViewById<MaterialButton>(R.id.btnMovies)
        button.setOnClickListener {
            MovieListActivity.start(this)
        }
    }

}