package io.fajarca.project.unittestingsample.calculator

class Calculator {
    fun sum(x : Int, y : Int) = x + y
    fun divide(x : Int, y : Int) = x / y
    fun multiply(x : Int, y : Int) = x * y
}