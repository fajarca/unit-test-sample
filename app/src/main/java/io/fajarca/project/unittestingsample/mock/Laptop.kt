package io.fajarca.project.unittestingsample.mock

class Laptop(private val spotify: Spotify, private val zoom: Zoom) {

    fun shutdown() {
        spotify.close()
        zoom.close()
    }

}