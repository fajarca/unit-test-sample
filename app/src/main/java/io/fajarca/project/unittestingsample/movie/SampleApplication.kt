package io.fajarca.project.unittestingsample.movie

import android.app.Activity
import android.app.Application
import androidx.lifecycle.LifecycleObserver
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.fajarca.project.unittestingsample.movie.di.component.DaggerAppComponent
import javax.inject.Inject


class SampleApplication :
    Application(),
    HasActivityInjector,
    LifecycleObserver {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initAppDependencyInjection()
    }

    private fun initAppDependencyInjection() {
        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

    override fun activityInjector() = activityInjector

}
