package io.fajarca.project.unittestingsample.movie.abstraction

interface Mapper<in I, out O> {
    fun map(input: I): O
}
