package io.fajarca.project.unittestingsample.movie.abstraction

interface UseCase<Params, out T> {
    suspend fun execute(params: Params): T

    object None
}
