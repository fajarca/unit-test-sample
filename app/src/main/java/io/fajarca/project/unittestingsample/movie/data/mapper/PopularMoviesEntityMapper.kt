package io.fajarca.project.unittestingsample.movie.data.mapper

import io.fajarca.project.unittestingsample.movie.abstraction.Mapper
import io.fajarca.project.unittestingsample.movie.data.db.MovieEntity
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie
import javax.inject.Inject

class PopularMoviesEntityMapper @Inject constructor() : Mapper<List<Movie>, List<MovieEntity>> {

    override fun map(input: List<Movie>): List<MovieEntity> {
        return input.map { movie ->
            MovieEntity(movie.id, movie.title, movie.imageUrl)
        }
    }

}