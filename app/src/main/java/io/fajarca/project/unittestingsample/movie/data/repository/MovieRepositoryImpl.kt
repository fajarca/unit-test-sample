package io.fajarca.project.unittestingsample.movie.data.repository

import io.fajarca.project.unittestingsample.movie.data.dispatcher.DispatcherProvider
import io.fajarca.project.unittestingsample.movie.data.mapper.PopularMoviesEntityMapper
import io.fajarca.project.unittestingsample.movie.data.mapper.PopularMoviesMapper
import io.fajarca.project.unittestingsample.movie.data.source.MovieLocalDataSource
import io.fajarca.project.unittestingsample.movie.data.source.MovieRemoteDataSource
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie
import io.fajarca.project.unittestingsample.movie.domain.repository.MovieRepository
import javax.inject.Inject
import io.fajarca.project.unittestingsample.movie.domain.Result

class MovieRepositoryImpl @Inject constructor(
    private val dispatcher: DispatcherProvider,
    private val mapper: PopularMoviesMapper,
    private val entityMapper: PopularMoviesEntityMapper,
    private val remoteDataSource: MovieRemoteDataSource,
    private val localDataSource: MovieLocalDataSource
) : MovieRepository {


    override suspend fun getPopularMovies(): Result<List<Movie>> {
        val apiResult = remoteDataSource.getPopularMovies(dispatcher.io)
        return when (apiResult) {
            is Result.Success -> {
                val movies = mapper.map(apiResult.data)
                localDataSource.insertAll(entityMapper.map(movies))
                Result.Success(movies)
            }
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

}