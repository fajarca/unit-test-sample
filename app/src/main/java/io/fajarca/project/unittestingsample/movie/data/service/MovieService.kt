package io.fajarca.project.unittestingsample.movie.data.service

import io.fajarca.project.unittestingsample.movie.data.response.GetPopularMoviesDto
import retrofit2.http.GET

interface MovieService {
    @GET("movie/popular")
    suspend fun getPopularMovies() : GetPopularMoviesDto
}