package io.fajarca.project.unittestingsample.movie.data.source

import io.fajarca.project.unittestingsample.movie.data.db.MovieDatabase
import io.fajarca.project.unittestingsample.movie.data.db.MovieEntity
import javax.inject.Inject


class MovieLocalDataSource @Inject constructor(
    private val database: MovieDatabase
) {

    suspend fun insertAll(movies : List<MovieEntity>) {
        database.movieDao().insertAll(*movies.toTypedArray())
    }

}