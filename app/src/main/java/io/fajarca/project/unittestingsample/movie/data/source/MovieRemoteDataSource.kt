package io.fajarca.project.unittestingsample.movie.data.source

import io.fajarca.project.unittestingsample.movie.data.response.GetPopularMoviesDto
import io.fajarca.project.unittestingsample.movie.data.service.MovieService
import javax.inject.Inject
import kotlinx.coroutines.CoroutineDispatcher
import io.fajarca.project.unittestingsample.movie.domain.Result

class MovieRemoteDataSource @Inject constructor(
    private val movieService: MovieService
) : RemoteDataSource() {

    suspend fun getPopularMovies(dispatcher: CoroutineDispatcher): Result<GetPopularMoviesDto> {
        return safeApiCall(dispatcher) { movieService.getPopularMovies() }
    }

}