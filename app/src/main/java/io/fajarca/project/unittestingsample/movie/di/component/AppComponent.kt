package io.fajarca.project.unittestingsample.movie.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import io.fajarca.project.unittestingsample.movie.SampleApplication
import io.fajarca.project.unittestingsample.movie.di.module.ActivityBuilder
import io.fajarca.project.unittestingsample.movie.di.module.ApiServiceModule
import io.fajarca.project.unittestingsample.movie.di.module.AppModule
import io.fajarca.project.unittestingsample.movie.di.module.CoroutineDispatcherModule
import io.fajarca.project.unittestingsample.movie.di.module.DatabaseModule
import io.fajarca.project.unittestingsample.movie.di.module.NetworkModule
import io.fajarca.project.unittestingsample.movie.di.module.RepositoryModule
import io.fajarca.project.unittestingsample.movie.di.module.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        AndroidInjectionModule::class,
        ActivityBuilder::class,
        ViewModelModule::class,
        ApiServiceModule::class,
        CoroutineDispatcherModule::class,
        DatabaseModule::class
    ]
)

interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: SampleApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: SampleApplication)
}
