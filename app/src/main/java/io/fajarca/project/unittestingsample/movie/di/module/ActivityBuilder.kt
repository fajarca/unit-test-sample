package io.fajarca.project.unittestingsample.movie.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.fajarca.project.unittestingsample.movie.presentation.MovieListActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun contributesMovieListActivity(): MovieListActivity

}
