package io.fajarca.project.unittestingsample.movie.di.module

import dagger.Module
import dagger.Provides
import io.fajarca.project.unittestingsample.movie.data.service.MovieService
import javax.inject.Singleton
import retrofit2.Retrofit

@Module
class ApiServiceModule {

    @Provides
    @Singleton
    fun provideMovieService(retrofit: Retrofit) = retrofit.create(MovieService::class.java)

}
