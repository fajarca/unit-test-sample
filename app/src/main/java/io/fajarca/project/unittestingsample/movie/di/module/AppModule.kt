package io.fajarca.project.unittestingsample.movie.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.fajarca.project.unittestingsample.movie.SampleApplication
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: SampleApplication): Context = app

    @Provides
    @Singleton
    fun provideApplications(app: SampleApplication): Application = app

}
