package io.fajarca.project.unittestingsample.movie.di.module

import dagger.Binds
import dagger.Module
import io.fajarca.project.unittestingsample.movie.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.unittestingsample.movie.data.dispatcher.DispatcherProvider

@Module
interface CoroutineDispatcherModule {
    @Binds
    fun bindDispatcher(dispatcherProvider: CoroutineDispatcherProvider): DispatcherProvider
}
