package io.fajarca.project.unittestingsample.movie.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.fajarca.project.unittestingsample.movie.data.db.MovieDao
import io.fajarca.project.unittestingsample.movie.data.db.MovieDatabase
import javax.inject.Singleton

@Module
class DatabaseModule {

    companion object {
        private const val DATABASE_NAME = "app_database"
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Context): MovieDatabase {
        return Room.databaseBuilder(
            context,
            MovieDatabase::class.java,
            DATABASE_NAME
        ).build()
    }

    @Provides
    fun provideMovieDao(database: MovieDatabase): MovieDao {
        return database.movieDao()
    }


}