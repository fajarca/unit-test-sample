package io.fajarca.project.unittestingsample.movie.di.module

import dagger.Binds
import dagger.Module
import io.fajarca.project.unittestingsample.movie.data.repository.MovieRepositoryImpl
import io.fajarca.project.unittestingsample.movie.domain.repository.MovieRepository

@Module
interface RepositoryModule {
    @Binds
    fun bindRepository(repository: MovieRepositoryImpl): MovieRepository
}
