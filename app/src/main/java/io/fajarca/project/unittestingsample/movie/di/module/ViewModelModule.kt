package io.fajarca.project.unittestingsample.movie.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.fajarca.project.unittestingsample.movie.di.annotation.ViewModelKey
import io.fajarca.project.unittestingsample.movie.di.factory.ViewModelFactory
import io.fajarca.project.unittestingsample.movie.presentation.MovieListViewModel

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    internal abstract fun provideMovieListViewModel(
        viewModel: MovieListViewModel
    ): ViewModel

}
