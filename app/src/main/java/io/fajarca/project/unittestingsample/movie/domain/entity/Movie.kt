package io.fajarca.project.unittestingsample.movie.domain.entity

data class Movie(val id : Int, val title : String, val imageUrl : String)