package io.fajarca.project.unittestingsample.movie.domain.repository

import io.fajarca.project.unittestingsample.movie.domain.Result
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie

interface MovieRepository {
    suspend fun getPopularMovies(): Result<List<Movie>>
}