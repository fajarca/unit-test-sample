package io.fajarca.project.unittestingsample.movie.domain.usecase

import io.fajarca.project.unittestingsample.movie.abstraction.UseCase
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie
import io.fajarca.project.unittestingsample.movie.domain.repository.MovieRepository
import javax.inject.Inject
import io.fajarca.project.unittestingsample.movie.domain.Result

class GetPopularMoviesUseCase @Inject constructor(
    private val repository: MovieRepository
) : UseCase<UseCase.None, Result<List<Movie>>> {

    override suspend fun execute(params: UseCase.None): Result<List<Movie>> {
        return repository.getPopularMovies()
    }

}