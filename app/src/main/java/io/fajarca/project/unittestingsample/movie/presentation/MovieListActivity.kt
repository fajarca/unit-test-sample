package io.fajarca.project.unittestingsample.movie.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import io.fajarca.project.unittestingsample.R
import io.fajarca.project.unittestingsample.databinding.ActivityMovieListBinding
import io.fajarca.project.unittestingsample.movie.abstraction.BaseActivity
import io.fajarca.project.unittestingsample.movie.domain.Result
import io.fajarca.project.unittestingsample.movie.util.gone
import io.fajarca.project.unittestingsample.movie.util.visible


class MovieListActivity : BaseActivity<ActivityMovieListBinding, MovieListViewModel>() {

    override fun getLayoutResourceId() = R.layout.activity_movie_list
    override fun getViewModelClass() = MovieListViewModel::class.java

    private val adapter by lazy { MovieRecyclerAdapter() }

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, MovieListActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupRecyclerView()
        observePopularMovies()
        vm.getPopularMovies()
    }

    private fun setupRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
        adapter.setOnMovieSelected { movie ->

        }

    }

    private fun observePopularMovies() {
        vm.popularMovies.observe(this) {
            when (it) {
                Result.Loading -> {
                    binding.progressBar.visible()
                }
                is Result.Success -> {
                    binding.progressBar.gone()
                    adapter.submitList(it.data)
                }
                is Result.Error -> {
                    binding.progressBar.gone()
                }
            }
        }
    }

}