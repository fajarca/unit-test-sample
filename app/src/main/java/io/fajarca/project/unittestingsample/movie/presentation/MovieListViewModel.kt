package io.fajarca.project.unittestingsample.movie.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.project.unittestingsample.movie.abstraction.UseCase
import io.fajarca.project.unittestingsample.movie.domain.Result
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie
import io.fajarca.project.unittestingsample.movie.domain.usecase.GetPopularMoviesUseCase
import javax.inject.Inject
import kotlinx.coroutines.launch


class MovieListViewModel @Inject constructor(
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase
) : ViewModel() {

    private val _popularMovies = MutableLiveData<Result<List<Movie>>>()
    val popularMovies: LiveData<Result<List<Movie>>>
        get() = _popularMovies

    fun getPopularMovies() {
        _popularMovies.value = Result.Loading
        viewModelScope.launch {
            _popularMovies.value = getPopularMoviesUseCase.execute(UseCase.None)
        }
    }


}