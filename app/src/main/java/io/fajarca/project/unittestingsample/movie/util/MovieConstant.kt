package io.fajarca.project.unittestingsample.movie.util

object MovieConstant {
    const val IMAGE_BASE_URL_POSTER = "https://image.tmdb.org/t/p/w500/"
    const val GLIDE_THUMBNAIL_SIZE_MULTIPLIER = 0.1f
}