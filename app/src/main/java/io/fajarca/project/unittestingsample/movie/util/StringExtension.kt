package io.fajarca.project.unittestingsample.movie.util

import io.fajarca.project.unittestingsample.movie.util.MovieConstant.IMAGE_BASE_URL_POSTER

fun String.createImageUrl(): String {
    return IMAGE_BASE_URL_POSTER + this
}