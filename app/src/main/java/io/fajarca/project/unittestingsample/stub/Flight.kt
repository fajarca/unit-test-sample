package io.fajarca.project.unittestingsample.stub

data class Flight(val destination : String, val isActive : Boolean)