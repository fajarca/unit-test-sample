package io.fajarca.project.unittestingsample.stub

class FlightDatabase {

    /**
     * For demonstration purpose, we will provide the data by using dummy flight data.
     */
    fun activeFlights(): List<Flight> {
        return listOf(
            Flight("Lombok", true),
            Flight("Bali", true),
            Flight("Papua", false)
        )
    }

}