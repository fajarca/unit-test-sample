package io.fajarca.project.unittestingsample.stub

class FlightTicketVendingMachine(private val database: FlightDatabase) {

    fun findActiveFlightsCount() : Int {
        val flights = database.activeFlights()
        return flights.filter { it.isActive }.size
    }

}