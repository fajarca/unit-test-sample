package io.fajarca.project.unittestingsample.calculator

import org.junit.Assert.*
import org.junit.Test

class CalculatorTest {

    private val calculator = Calculator()

    @Test
    fun givenTwoNumbers_whenSumInvoked_shouldReturnValidSumResult() {
        val expected = 4
        val actual = calculator.sum(2,2)
        assertEquals(expected, actual)
    }

    @Test
    fun givenTwoNumbers_whenDivideInvoked_shouldReturnValidDivideResult() {
        val expected = 4
        val actual = calculator.divide(16,4)
        assertEquals(expected, actual)
    }

    @Test
    fun givenTwoNumbers_whenMultiplicationInvoked_shouldReturnValidMultiplicationResult() {
        val expected = 25
        val actual = calculator.multiply(5,5)
        assertEquals(expected, actual)
    }
}