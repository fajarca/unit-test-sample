package io.fajarca.project.unittestingsample.helper.extension

import io.fajarca.project.unittestingsample.helper.rule.CoroutineTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest

@ExperimentalCoroutinesApi
fun CoroutineTestRule.runBlockingTest(block : suspend TestCoroutineScope.() -> Unit) {
    testDispatcher.runBlockingTest(block)
}