package io.fajarca.project.unittestingsample.mock

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class LaptopTest {

    @Test
    fun whenShuttingDownLaptop_shouldCloseSpotifyAndZoomApp() {
        //Given
        val spotify: Spotify = mockk()
        val zoom: Zoom = mockk()
        val laptop = Laptop(spotify, zoom)

        every { spotify.close() } returns Unit //Stubbing
        every { zoom.close() } returns Unit //Stubbing

        //When
        laptop.shutdown()

        //Then
        verify { zoom.close() } //Mocking
        verify { spotify.close() } //Mocking
    }
}