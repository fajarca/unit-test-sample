package io.fajarca.project.unittestingsample.movie.data.repository

import io.fajarca.project.unittestingsample.helper.extension.runBlockingTest
import io.fajarca.project.unittestingsample.helper.rule.CoroutineTestRule
import io.fajarca.project.unittestingsample.movie.data.mapper.PopularMoviesEntityMapper
import io.fajarca.project.unittestingsample.movie.data.mapper.PopularMoviesMapper
import io.fajarca.project.unittestingsample.movie.data.response.GetPopularMoviesDto
import io.fajarca.project.unittestingsample.movie.data.source.MovieLocalDataSource
import io.fajarca.project.unittestingsample.movie.data.source.MovieRemoteDataSource
import io.fajarca.project.unittestingsample.movie.domain.Result
import io.mockk.Called
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MovieRepositoryImplTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private val mapper: PopularMoviesMapper = mockk(relaxed = true)
    private val entityMapper: PopularMoviesEntityMapper = mockk(relaxed = true)
    private val remoteDataSource: MovieRemoteDataSource = mockk()
    private val localDataSource: MovieLocalDataSource = mockk(relaxed = true)


    private val repository =
        MovieRepositoryImpl(
            coroutineTestRule.testDispatcherProvider,
            mapper,
            entityMapper,
            remoteDataSource,
            localDataSource
        )

    @Test
    fun whenGetPopularMovies_shouldGetPopularMoviesFromNetwork() =
        coroutineTestRule.runBlockingTest {
            //Given
            val dummyMovies = populateDummyPopularMoviesResponse()

            coEvery { remoteDataSource.getPopularMovies(coroutineTestRule.testDispatcher) } returns Result.Success(
                dummyMovies
            )

            //When
            repository.getPopularMovies()

            //Then
            coVerify { remoteDataSource.getPopularMovies(coroutineTestRule.testDispatcher) }
        }

    @Test
    fun whenGetPopularMoviesFromRemoteSuccess_shouldSaveDataToDatabase() =
        coroutineTestRule.runBlockingTest {
            //Given
            val dummyMovies = populateDummyPopularMoviesResponse()

            coEvery { remoteDataSource.getPopularMovies(coroutineTestRule.testDispatcher) } returns Result.Success(
                dummyMovies
            )

            //When
            repository.getPopularMovies()

            //Then
            coVerify { localDataSource.insertAll(any()) }
        }


    @Test
    fun whenGetPopularMoviesFromRemoteFailed_neverSaveDataToDatabase() =
        coroutineTestRule.runBlockingTest {
            //Given
            val dummyMovies = populateDummyPopularMoviesResponse()

            coEvery { remoteDataSource.getPopularMovies(coroutineTestRule.testDispatcher) } returns Result.Error()

            //When
            repository.getPopularMovies()

            //Then
            coVerify(exactly = 0) { localDataSource.insertAll(any()) }
        }

    private fun populateDummyPopularMoviesResponse(): GetPopularMoviesDto {
        return GetPopularMoviesDto()
    }
}