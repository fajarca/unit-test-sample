package io.fajarca.project.unittestingsample.movie.domain.usecase

import io.fajarca.project.unittestingsample.helper.extension.runBlockingTest
import io.fajarca.project.unittestingsample.helper.rule.CoroutineTestRule
import io.fajarca.project.unittestingsample.movie.abstraction.UseCase
import io.fajarca.project.unittestingsample.movie.domain.Result
import io.fajarca.project.unittestingsample.movie.domain.repository.MovieRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GetPopularMoviesUseCaseTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private val repository : MovieRepository = mockk()
    private val useCase = GetPopularMoviesUseCase(repository)

    @Test
    fun whenGetPopularMoviesInvoked_shouldGetDataFromRepository() = coroutineTestRule.runBlockingTest {

        coEvery { repository.getPopularMovies() } returns Result.Success(emptyList())

        //When
        useCase.execute(UseCase.None)

        //Then
        coVerify { repository.getPopularMovies() }
    }
}