package io.fajarca.project.unittestingsample.movie.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.fajarca.project.unittestingsample.helper.rule.CoroutineTestRule
import io.fajarca.project.unittestingsample.movie.abstraction.UseCase
import io.fajarca.project.unittestingsample.movie.domain.HttpResult
import io.fajarca.project.unittestingsample.movie.domain.Result
import io.fajarca.project.unittestingsample.movie.domain.entity.Movie
import io.fajarca.project.unittestingsample.movie.domain.usecase.GetPopularMoviesUseCase
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class MovieListViewModelTest {


    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val useCase: GetPopularMoviesUseCase = mockk()
    private val observer: Observer<Result<List<Movie>>> = spyk()
    private val viewModel = MovieListViewModel(useCase)

    @Before
    fun setup() {
        viewModel.popularMovies.observeForever(observer)
    }

    @Test
    fun whenFetchPopularMovieSuccess_shouldEmitSuccessToObserver() {
        //Given
        val movies = populateDummyMovie()
        coEvery { useCase.execute(UseCase.None) } returns Result.Success(movies)

        //When
        viewModel.getPopularMovies()

        //Then
        verify {
            observer.onChanged(Result.Loading)
            observer.onChanged(Result.Success(movies))
        }
    }

    @Test
    fun whenFetchPopularMovieFailed_shouldEmitErrorToObserver() {
        //Given

        coEvery { useCase.execute(UseCase.None) } returns Result.Error(cause = HttpResult.TIMEOUT)

        //When
        viewModel.getPopularMovies()

        //Then
        verify {
            observer.onChanged(Result.Loading)
            observer.onChanged(Result.Error(cause = HttpResult.TIMEOUT))
        }
    }

    private fun populateDummyMovie(): List<Movie> {
        return listOf(
            Movie(1, "John Wick 3", "some-url"),
            Movie(2, "Money Heist", "some-url")
        )
    }

}