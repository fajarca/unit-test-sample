package io.fajarca.project.unittestingsample.stub

import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test

class FlightTicketVendingMachineTest {

    @Test
    fun givenListOfActiveFlights_whenFindActiveFlight_returnNumberOfActiveFlight() {
        //Given
        val database : FlightDatabase  = mockk()
        val vendingMachine = FlightTicketVendingMachine(database)

        //Stub
        every { database.activeFlights() } returns listOf(Flight("Lombok", true), Flight("Bali", true))

        //When
        val actual = vendingMachine.findActiveFlightsCount()

        //Then
        assertEquals(2, actual)
    }

    @Test
    fun givenListOfInactiveFlights_whenFindActiveFlight_returnNoActiveFlight() {
        //Given
        val database : FlightDatabase  = mockk()
        val vendingMachine = FlightTicketVendingMachine(database)

        //Stub
        every { database.activeFlights() } returns listOf(Flight("Lombok", false), Flight("Bali", false))

        //When
        val actual = vendingMachine.findActiveFlightsCount()

        //Then
        assertEquals(0, actual)
    }
}